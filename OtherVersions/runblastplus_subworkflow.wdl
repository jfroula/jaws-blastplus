workflow runblastplus_sub {
    meta {
        version: '1.0.0'
        author: 'Stephan Trong, Jeff Froula'
    }

    ### from inputs.json:
    File queryFile
    String refDb
    String refOutputName
    Int chunk_size
    String params
    
    call shard {
        input: query = queryFile,
        chunk_size=chunk_size
    }

    Array[String] shard_array = shard.shards

    # For each ref file defined in the inputs json, blast query with ref in parallel.
    #
    scatter(coords in shard_array) {
        call runBlast{
            input: query=queryFile,
                   prefix=refOutputName,
                   db=refDb,
                   blast_options=params,
                   coords=coords
        }
    }

    call mergeBlast{
        input: blastout_array=runBlast.blastout,
               prefix=refOutputName
    }

    output {
       File blastout = mergeBlast.merged
    }
}

### ------------------------------------ ###
task shard {
    File query
    String bname = basename(query)
    Int chunk_size

    command {
        set -e -o pipefail
        shifter --image=jfroula/jaws-sharding:1.0.10 fasta_indexer.py --input ${query} --output ${bname}.index

        shifter --image=jfroula/jaws-sharding:1.0.10 create_blocks.py -if ${bname}.index -ff ${query} -of ${bname}.sharded -bs ${chunk_size}
    }

    output {
        Array[String] shards = read_lines("${bname}.sharded")
    }
}

task runBlast {
    File query
    String prefix
    String db
    String coords
    String blast_options


    command <<< 
        start=$(echo ${coords} | awk '{print $1}')
        end=$(echo ${coords} | awk '{print $2}')
      
        #DEFAULT_OUTFMT = " -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles' "
        #DEFAULT_BLAST_OPTIONS = " -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true -num_alignments 5 %s " % (DEFAULT_OUTFMT)
        #FUNGAL_BLAST_OPTIONS = " -task megablast -perc_identity 90 -evalue 1e-30 -dust \"yes\" -num_threads 8 %s " % (DEFAULT_OUTFMT)

        # TODO jfroula: we need to add a preceding space to blast_options because 
        # a bug in argparse won't recognize arguments begining with "-" (i.e. -p "-num_threads 8")

        # we are piping a block of the fastq sequence to the aligner 
        shifter --image=jfroula/jaws-sharding:1.0.10 shard_reader.py -i ${query} -s $start -e $end | \
        shifter --image=jfroula/jaws-blastplus:1.0.18 run_blast.py -db ${db} \
          --query_seq -  \
          --query_file ${query}  \
          --output-name ${prefix} \
          ${blast_options}
          
    >>>

    output {
        File blastout = "${prefix}.parsed"
    }

    runtime {
        poolname: "medium" 
    }

#   For dynamic pool configuration
#    runtime {
#        cluster: "cori"
#        time: "00:30:00"
#        cpu: 1 
#        mem: "5G"
#        poolname: "test"
#        poolsize: 1
#        backend: "JTM"
#    }
}

task mergeBlast{
    Array[File] blastout_array
    String prefix
    command {
        # merge and sort by evalue
        cat ${sep=' ' blastout_array} | sort -k4,4 > ${prefix}.merged.fa
    }

    output{
        File merged = "${prefix}.merged.fa"
    }
}
