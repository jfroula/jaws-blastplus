import "blastn_sub.wdl" as blastplus_sub
workflow runblastplus_main {
    meta {
        version: '1.0.0'
        author: 'Stephan Trong,Jeff Froula'
    }

    # from inputs.json:
    File queryFile
    Map[String, String] refDb
    Map[String, String] refOutputName
    String params
    Int chunk_size

    call parseRefDbPath {
        input: db_full=RefDb
    }
    
    # For each ref file defined in the inputs json, blast query with ref in parallel.
    scatter (pair in refDb) {
        call makeBlastDb {
            input: dbpath=pair.right
        }
        call blastplus_sub.runblastplus_sub{
            input: params=params,
                   queryFile=queryFile,
                   refOutputName=refOutputName[pair.left],
                   refDb_dir=makeBlastDb.db_index_files_dir,
                   refDb_prefix=makeBlastDb.db_index_files_dir,
                   chunk_size=chunk_size
        }

        call taxon_processing{
            input: query=queryFile,
                   params=params,
                   blastout=runblastplus_sub.blastout,
                   prefix=refOutputName[pair.left],
                   refDb=makeBlastDb.db_index_files_dir,
        }
    }
}

### ------------------------------ ###

task parseRefDbPath {
    String db_full

    command {
	    # refDb path should include prefix "nt" (i.e. /global/dna/shared/nt/nt)
        # if the db exists but not include it (i.e. /global/dna/shared/nt) if we should
        # make db here.

        # so basename should get "nt" which will be directory name or "prefix"
        String db_prefix = basename(refDb)
        File db_dir = sub(refDb, db_prefix, "" )
    }

    runtime {
    }

    output {
    }
}

task makeBlastDb {
    String makeBlastDbCmd 
    File db_dir
    String db_prefix
    String bname=basename(db_dir)

    runtime {
        poolname: "medium"
    }

    command {
        python <<CODE
        import os, sys, glob

        db_full=''
        if ${dbprefix}:
            db_full=${db_dir}+"/"+${db_prefix}
        else: 
            db_full=${db_dir}+"/"+${bname}

        tmp = open("tmp_path","w")

        full_db_path = os.environ.get('${db_dir}')

        if not full_db_path:
            sys.exit("Failed to get path to database. Maybe environmental variable not exported for %s"%'${dbpath}')

        # does the database already exist?
        if glob.glob(full_db_path + '*.nin'):
            # If we are here, the dbpath should not have a suffix like .fa
            # Write the path to database (and its prefix) as this will be 
            # the output from this task
            tmp.write(full_db_path)
            tmp.close()
        else:
            # If we are here, the dbpath should end in a suffix [fa|fasta|fna]
            fname = os.path.basename(full_db_path)  # nt.fa
            dirname = os.path.dirname(full_db_path)
            prefix = os.path.splitext(fname)        # ["nt",fa]
            name = prefix[0]                        # nt
            os.mkdir(name,0o755)
            makeBlastDbCmd = "${makeBlastDbCmd}" + " -in %s -out %s 1>makeblastdb.log" % \
              (os.path.join(name,fname),os.path.join(name,name))
            os.symlink(full_db_path, os.path.join(name,fname))
            os.system(makeBlastDbCmd)

            # Write the path to database (and its prefix) as this will be 
            # the output from this task
            tmp.write(os.path.join(os.getcwd(),name,name))
            tmp.close()
        CODE
    }
    output {
        String db_index_files_dir = read_string("tmp_path") 
    }
}

task taxon_processing{
    File query
    File blastout
    String prefix
    String refDb
    String params

    command {
        docker run jfroula/jaws-blastplus:1.0.10 \
        run_blastplus_postprocessing.py -q ${query} -bo ${blastout} -d ${refDb} -o "./${prefix}"
    }
    output {
        File taxa_files = "${prefix}"
    }
}
