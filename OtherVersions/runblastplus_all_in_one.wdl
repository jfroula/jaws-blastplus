workflow runblastplus_all_in_one{
    meta {
        version: '1.0.0'
        author: 'Stephan Trong,Jeff Froula'
    }


    ### from inputs.json:
    File queryFile
    Map[String, String] refDb
    Map[String, String] refOutputName
    String params
    
    # For each ref file defined in the inputs json, blast query with ref in parallel.
    #
    scatter (pair in refDb) {
        call makeBlastDb {
            input: dbpath=pair.right
        }
        call blastplus{
            input: params=params,
                   query=queryFile,
                   prefix=refOutputName[pair.left],
                   refDb=makeBlastDb.db_index_files_dir
        }
    }
}

### ------------------------------ ###

task makeBlastDb {
    String dbpath
    String makeBlastDbCmd 

    runtime {
        poolname: "medium"
    }

    command {
        python <<CODE
        import os, sys, glob

        tmp = open("tmp_path","w")

        full_db_path = os.environ.get('${dbpath}')

        # I need the environmental variable name without the '$' 
        # does the database already exist?
        if glob.glob(full_db_path + '*.nin'):
            # If we are here, the dbpath should not have a suffix like .fa
            # Write the path to database (and its prefix) as this will be 
            # the output from this task
            tmp.write(full_db_path)
            tmp.close()
        else:
            # If we are here, the dbpath should end in a suffix [fa|fasta|fna]
            fname = os.path.basename(full_db_path)  # nt.fa
            dirname = os.path.dirname(full_db_path)
            prefix = os.path.splitext(fname)        # ["nt",fa]
            name = prefix[0]                        # nt
            os.mkdir(name,0755)
            makeBlastDbCmd = "${makeBlastDbCmd}" + " -in %s -out %s 1>makeblastdb.log" % \
              (os.path.join(name,fname),os.path.join(name,name))
            os.symlink(full_db_path, os.path.join(name,fname))
            os.system(makeBlastDbCmd)

            # Write the path to database (and its prefix) as this will be 
            # the output from this task
            tmp.write(os.path.join(os.getcwd(),name,name))
            tmp.close()
        CODE
    }
    output {
        String db_index_files_dir = read_string("tmp_path") 
    }
}

task blastplus{
    File query
    String prefix
    String refDb
    String params

    command {
		shifter --image=jfroula/jaws-blastplus:1.0.18 \
		run_blastplus_taxserver.py \
		-q ${query} \
		-d ${refDb} \
		-c ${prefix} \
		-o . -n -f -s ${params}
    }

    output {
        #File taxa_files = "${prefix}"
    }
}
