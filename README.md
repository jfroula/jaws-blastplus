# Summary
This is a repository for the run_blastplus_taxonserver WDL.  This implementation of blast uses sharding. Sharding is when the query fasta is split up into blocks and passed to blast via pipes and not files.  This is fast and performs blast in parallel.

# Test run
### Using conda
```
conda activate /global/homes/j/jfroula/.conda/envs/blastplus
run_blastplus_taxserver.py -c outfile -p="num_threads=8"\
  -d /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nt/nt \
  -q /global/cscratch1/sd/jfroula/JAWS/jaws-blastplus/data/short.fa
```

### or using shifter
```
shifter --image=jfroula/jaws-blastplus:1.0.2 \
  run_blastplus_taxserver.py -c outfile -p="num_threads=8"\
  -d /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nt/nt \
  -q /global/cscratch1/sd/jfroula/JAWS/jaws-blastplus/data/short.fa
```