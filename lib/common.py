#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Common function library


"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


from __future__ import division, absolute_import, print_function, unicode_literals
#from builtins import *
#from future.builtins.disabled import *

import re
import os
import time
import sys
import requests
import logging
import json
from logging import handlers
from subprocess import Popen, PIPE
from email.mime.text import MIMEText

my_path = os.path.dirname(__file__)

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions


'''
creates a logging instance
https://docs.python.org/2/howto/logging.html
https://pypi.python.org/pypi/colorlog
* color log not working so I removed that option
'''
def get_logger(log_name, log_file, log_level="INFO", stdout=False, color=False, file_and_console=False):
    log = logging.getLogger(log_name)
    handler = None

    formatter = logging.Formatter('%(filename)-15s:%(process)d %(asctime)s %(levelname)s: %(message)s')

    def add_handler(handler, formatter):
        handler.setFormatter(formatter)
        log.addHandler(handler)

    if file_and_console:
        add_handler(logging.StreamHandler(sys.stdout), formatter)
        add_handler(handlers.RotatingFileHandler(log_file, maxBytes=(1048576*5), backupCount=7),  formatter)

    elif stdout:
        add_handler(logging.StreamHandler(sys.stdout), formatter)

    else:
        add_handler(logging.FileHandler(log_file), formatter)

    # log.addHandler(handler)
    log.setLevel(log_level)

    return log


'''
Checkpoint the status plus a timestamp
- appends the status

@param status_log: /path/to/status.log (or whatever you name it)
@param status: status to append to status.log

'''
def checkpoint_step(status_log, status):
    status_line = "%s,%s\n" % (status, time.strftime("%Y-%m-%d %H:%M:%S"))

    with open(status_log, "a") as myfile:
        myfile.write(status_line)

'''
returns the last step (status) from the pipeline
@param status_log: /path/to/status.log (or whatever you name it)
@param log: logger object

@return last status in the status log, "start" if nothing there
'''
def get_status(status_log, log=None):
    #status_log = "%s/%s" % (output_path, "test_status.log")

    status = "start"
    timestamp = str(time.strftime("%Y-%m-%d %H:%M:%S"))

    if os.path.isfile(status_log):
        fh = open(status_log, 'r')
        lines = fh.readlines()
        fh.close()

        for line in lines:
            if line.startswith('#'): continue
            line_list = line.split(",")
            assert len(line_list) == 2
            status = str(line_list[0]).strip()
            timestamp = str(line_list[1]).strip()

        if not status:
            status = "start"

        if log:
            log.info("Last checkpointed step: %s (%s)", status, timestamp)

    else:
        if log:
            log.info("Cannot find status.log (%s), assuming new run", status_log)

    status = status.strip().lower()

    return status



'''
run a command from python

@param cmd: command to run
@param live: False = run in dry mode (print command), True = run normally
@param log: logger object

@return std_out, std_err, exit_code

Still used in run_folder.py, rqc_fastq.py, rna_asm.py, rework_qc_helper.py, multi_alignment.py and others ...

'''
def run_command(cmd, live=False, log=None):
    stdOut = None
    stdErr = None
    exitCode = None
    #end = 0
    #elapsedSec = 0

    if cmd:
        if not live:
            stdOut = "Not live: cmd = '%s'" % (cmd)
            exitCode = 0
        else:
            if log: log.info("cmd: %s" % (cmd))

            p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
            stdOut, stdErr = p.communicate()
            exitCode = p.returncode
            print("MYEXITCOE %s"%exitCode)

        if log:
            log.info("Return values: exitCode=" + str(exitCode) + ", stdOut=" + str(stdOut) + ", stdErr=" + str(stdErr))
            if exitCode != 0:
                log.warn("- The exit code has non-zero value.")

    else:
        if log:
            log.error("- No command to run.")
            return None, None, -1

    return stdOut, stdErr, exitCode


'''
replacement for run_command
- includes logging, convert_cmd & post_mortem
'''

def run_cmd(cmd, log=None):

    std_out = None
    std_err = None
    exit_code = 0

    if cmd:
        # convert to work on genepool/denovo
        #cmd = convert_cmd(cmd, log)

        if log:
            log.info("- cmd: %s", cmd)

        p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        std_out, std_err = p.communicate()
        exit_code = p.returncode

        # check for docker error - could reset automatically for the "FAILED to lookup"
        if exit_code > 0:

            shifter_flag = False # write to log file
            if "FAILED to lookup docker image" in std_err:
                exit_code = 44
                shifter_flag = True

            # Julie K helped with this regex, much easier than splitting and parsing
            if exit_code != 44:
                match = re.search(r" > (\S*)(\s*)", cmd)
                if match:
                    log_file = match.group(1)
                    if log:
                        log.info("- checking log: %s", log_file)
                    exit_code = bbtools_log_check(log_file) # also looks for "FAILED to lookup docker image"

                    if exit_code == 44:
                        shifter_flag = True

            # log Shifter failures
            # if shifter_flag:

            #     shifter_fail = "/global/projectb/scratch/qc_user/rqc/prod/logs/shifter_fail.log"
            #     fh = open(shifter_fail, "a")
            #     timestamp = str(time.strftime("%Y-%m-%d %H:%M:%S"))
            #     slurm_job_id = os.environ.get('SLURM_JOB_ID', 'unknown') # also SLURM_JOBID?
            #     slurm_job_name = os.environ.get('SLURM_JOB_NAME', 'unknown')
            #     fh.write("%s|%s|%s|%s\n" % (timestamp, slurm_job_id, slurm_job_name, cmd))
            #     fh.close()



        post_mortem_cmd(cmd, exit_code, std_out, std_err, log)

    return std_out, std_err, exit_code


'''
Simple function to output to the log what happened only if exit code > 0
- used with run_cmd only if exit code > 0
'''
def post_mortem_cmd(cmd, exit_code, std_out, std_err, log=None):

    if exit_code > 0:

        if log:
            log.error("- cmd failed: %s", cmd)
            log.error("- exit code: %s", exit_code)

        else:
            print ("- cmd failed: %s" % (cmd))
            print ("- exit code: %s" % (exit_code))


        if std_out:
            if log:
                log.error("- std_out: %s", std_out)
            else:
                print ("- std_out: %s" % (std_out))

        if std_err:
            if log:
                log.error("- std_err: %s", std_err)
            else:
                print ("- std_err: %s" % (std_err))



'''
returns human readable file size
@param num = file size (e.g. 1000)

@return: readable float e.g. 1.5 KB
'''
def human_size(num):
    if not num:
        num = 0.0

    for x in ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'XB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0

    return "%3.1f %s" % (num, 'ZB')



'''
send out email
@param emailTo: email receipient (e.g. bryce@lbl.gov)
@param emailSubject: subject line for the email
@param emailBody: content of the email
@param emailFrom: optional email from

'''
def send_email(email_to, email_subject, email_body, email_from='rqc@jgi-psf.org', log=None):
    msg = ""
    err_flag = 0

    if not email_to:
        msg = "- send_email: email_to parameter missing!"

    if not email_subject:
        msg = "- send_email: email_subject parameter missing!"

    if not email_body:
        msg = "- send_email: email_body parameter missing!"


    if err_flag == 0:
        msg = "- sending email to: %s" % (email_to)

    if log:
        log.info(msg)
    else:
        print(msg)

    if err_flag == 1:
        return 0

    # assume html
    email_msg = MIMEText(email_body, "html") # vs "plain"
    email_msg['Subject'] = email_subject
    email_msg['From'] = email_from
    email_msg['To'] = email_to

    p = Popen(["/usr/sbin/sendmail", "-t"], stdin = PIPE)
    p.communicate(email_msg.as_string())

    return err_flag

'''
Write to rqc_file (e.g. rqc-files.tmp) the file_key and file_value

@param rqc_file_log: full path to file containing key=file
@param file_key: key for the entry
@param file_value: value for the entry

'''
def append_rqc_file(rqc_file_log, file_key, file_value, log=None):

    if file_key:
        my_buffer = "%s = %s\n" % (file_key, file_value)

        with open(rqc_file_log, "a") as myfile:
            myfile.write(my_buffer)

        if log:
            log.info("append_rqc_file: %s:%s", file_key, file_value)

    else:

        if log:
            log.warning("key or value error: %s:%s", file_key, file_value)


'''
Write to rqc_stats (e.g. rqc-stats.tmp) the stats_key and stats_value
@param rqc_file_log: full path to file containing key=file
@param file_key: key for the entry
@param file_value: value for the entry

'''
def append_rqc_stats(rqc_stats_log, stats_key, stats_value, log=None):
    if stats_key:

        buffer = "%s = %s\n" % (stats_key, stats_value)

        with open(rqc_stats_log, "a") as myfile:
            myfile.write(buffer)

        if log: log.info("append_rqc_stats: %s:%s" % (stats_key, stats_value))

    else:

        if log: log.warning("key or value error: %s:%s" % (stats_key, stats_value))

'''
Return the file system path to jgi-rqc-pipeline so we can use */tools and */lib

@return /path/to/jgi-rqc-pipelines
'''
def get_run_path():
    current_path = os.path.dirname(os.path.abspath(__file__))
    run_path = os.path.abspath(os.path.join(current_path, os.pardir))

    return run_path



'''
Simple read count using bbtools n_scaffolds field
- slightly different than in rqc_utility
n_scaffolds     n_contigs       scaf_bp contig_bp       gap_pct scaf_N50        scaf_L50        ctg_N50 ctg_L50 scaf_N90        scaf_L90        ctg_N90 ctg_L90 scaf_max        ctg_max scaf_n_gt50K     scaf_pct_gt50K  gc_avg  gc_std
1346616 1346616 405331416       405331415       0.000   1346616 301     1346615 301     1346616 301     1346615 301     301     301     0       0.000   0.44824 0.02675

'''
def get_read_count_fastq(fastq, log=None):
    read_cnt = 0

    if os.path.isfile(fastq):

        # bbtools faster than zcat | wc because bbtools uses pigz
        # cmd = "stats.sh format=3 in=%s" % fastq
        cmd = "stats.sh format=3 in=%s" % fastq
        std_out, std_err, exit_code = run_cmd(cmd, log)


        if exit_code == 0 and std_out:

            line_list = std_out.split("\n")
            #print line_list
            val_list = str(line_list[1]).split() #.split('\t')
            #print "v = %s" % val_list

            read_cnt = int(val_list[0]) # scaffolds

            if log:
                log.info("- read count: %s", read_cnt)


    else:
        log.error("- fastq: %s does not exist!", fastq)

    return read_cnt



'''
Subsampling calculation
0 .. 250k reads = 100%
250k .. 25m = 100% to 1%
25m .. 600m = 1%
600m+ .. oo < 1%

max subsample 6m reads

July 2014 - 15 runs > 600m (HiSeq-2500 Rapid) - 4 actual libraries / 85325 seq units
- returns new subsampling rate
'''
def get_subsample_rate(read_count):
    subsample = 0
    subsample_rate = 0.01
    max_subsample = 6000000 # 4 hours of blast time

    new_subsample_rate = 250000.0/read_count
    subsample_rate = max(new_subsample_rate, subsample_rate)
    subsample_rate = min(1, subsample_rate) # if subsample_rate > 1, then set to 1

    subsample = int(read_count * subsample_rate)

    if subsample > max_subsample:
        subsample = max_subsample

    subsample_rate = subsample / float(read_count)

    return subsample_rate


'''
function that just returns colors in a dictionary
'''
def get_colors():

    color = {
        'black' : "\033[1;30m",
        'red' : "\033[1;31m",
        'red2' : "\033[0;31m",
        'green' : "\033[1;32m",
        'yellow' : "\033[1;33m",
        'blue' : "\033[1;34m",
        'pink' : "\033[1;35m",
        'cyan' : "\033[1;36m",
        'white' : "\033[1;37m",
        '' : "\033[m"
    }


    return color



'''
Returns msg_ok, msg_fail, msg_warn colored or not colored
'''
def get_msg_settings(color):

    msg_ok = "[  "+color['green']+"OK"+color['']+"  ]"
    msg_fail = "[ "+color['red']+"FAIL"+color['']+" ]"
    msg_warn = "[ "+color['yellow']+"WARN"+color['']+" ]"

    return msg_ok, msg_fail, msg_warn


'''
Use RQC's ap_tool to get the status
set mode = "-sa" to show all, even completed
- this will need a hole opened in the firewall to use Rene's PMOS web svc
'''
def get_analysis_project_id(seq_proj_id, target_analysis_project_id, target_analysis_task_id, output_path, log=None, mode=""):

    if log:
        log.info("get_analysis_project_id: spid = %s, tapid = %s, tatid = %s", seq_proj_id, target_analysis_project_id, target_analysis_task_id)

    analysis_project_id = 0
    analysis_task_id = 0
    project_type = None
    task_type = None

    ap_list = os.path.join(output_path, "ap-info.txt")
    AP_TOOL = os.path.realpath(os.path.join(my_path, "../tools/ap_tool.py"))

    cmd = "%s -spid %s -m psv -tapid %s -tatid %s %s > %s 2>&1" % (AP_TOOL, seq_proj_id, target_analysis_project_id, target_analysis_task_id, mode, ap_list)
    if log:
        log.info("- cmd: %s", cmd)
    else:
        print ("- cmd: %s" % cmd)
    std_out, std_err, exit_code = run_command(cmd, True)

    post_mortem_cmd(cmd, exit_code, std_out, std_err, log)

    if os.path.isfile(ap_list):

        ap_dict = {} # header = value
        cnt = 0
        fh = open(ap_list, "r")
        for line in fh:
            arr = line.strip().split("|")
            if cnt == 0:
                c2 = 0 # position of title in header
                for a in arr:
                    ap_dict[a.lower()] = c2
                    c2 += 1

            else:

                for a in ap_dict:

                    if ap_dict[a] + 1 > len(arr):
                        pass
                    else:
                        ap_dict[a] = arr[ap_dict[a]]


            cnt += 1

        fh.close()


        analysis_project_id = ap_dict.get("analysis project id")
        analysis_task_id = ap_dict.get("analysis task id")
        project_type = ap_dict.get("analysis product name")
        task_type = ap_dict.get("analysis task name")

    # nno such project
    if cnt == 1:
        analysis_project_id = 0
        analysis_task_id = 0

    if log:
        log.info("- project type: %s, task type: %s", project_type, task_type)
        log.info("- analysis_project_id: %s, analysis_task_id: %s", analysis_project_id, analysis_task_id)

    try:
        analysis_project_id = int(analysis_project_id)
        analysis_task_id = int(analysis_task_id)
    except:
        analysis_project_id = 0
        analysis_task_id = 0


    # ap = 4, at = 8 means its using the column names but didn't find anything
    if analysis_project_id < 100:
        analysis_project_id = 0
    if analysis_task_id < 100:
        analysis_task_id = 0

    return analysis_project_id, analysis_task_id


'''
For creating a dot file from the pipeline flow
'''
def append_flow(flow_file, orig_node, orig_label, next_node, next_label, link_label):

    fh = open(flow_file, "a")
    fh.write("%s|%s|%s|%s|%s\n" % (orig_node, orig_label, next_node, next_label, link_label))
    fh.close()

'''
Flow file format:
# comment
*label|PBMID Pipeline run for BTXXY<br><font point-size="10">Run Date: 2017-09-28 14:22:50</font>
# origin node, origin label, next node, next label, link label
input_h5|BTXXY H5<br><font point-size="10">3 smrtcells</font>|assembly|HGAP Assembly<FONT POINT-SIZE="10"><br>3 contigs, 13,283,382bp</FONT>|HGAP v4.0.1

nodes should be the output of the transformation between the nodes
e.g. input fastq (25m reads) --[ bbtools subsampling ]--> subsampled fastq (10m reads)

creates a dot file, to convert to png use:
$ module load graphviz
$ dot -T png (dot file) > (png file)

More info on formatting the labels
http://www.graphviz.org/content/node-shapes#html
'''
def dot_flow(flow_file, dot_file, log=None):

    if not os.path.isfile(flow_file):
        if log:
            log.info("- cannot find flow file:  %s", flow_file)
        else:
            print ("Cannot find flow file: %s" % flow_file)
        return


    fhw = open(dot_file, "w")

    fhw.write("// dot file\n")
    fhw.write("digraph rqc {\n") # directed graph
    fhw.write("    node [shape=box];\n")
    fhw.write("    rankdir=LR;\n")

    fh = open(flow_file, "r")

    for line in fh:
        line = line.strip()

        if not line:
            continue

        if line.startswith("#"):
            continue

        # graph label
        if line.startswith("*label"):
            arr = line.split("|")
            label = flow_replace(str(arr[1]))

            fhw.write("    label=<%s>;\n" % label)
            fhw.write("    labelloc=top;\n")

        else:

            arr = line.split("|")
            #print arr

            if len(arr) == 5:

                org_node = arr[0]
                org_label = str(arr[1])
                next_node = arr[2]
                next_label = str(arr[3])
                link_label = str(arr[4])

                # must be <br/> in the dot file, I have a habit of using <br>
                org_label = flow_replace(org_label)
                next_label = flow_replace(next_label)
                link_label = flow_replace(link_label)

                # label are enclosed by < > instead of " " to handle html-ish markups

                if next_node:
                    link = "    %s -> %s;\n" % (org_node, next_node)
                    if link_label:
                        link = "    %s -> %s [label=<%s>];\n" % (org_node, next_node, link_label)
                    fhw.write(link)

                if org_label:
                    label = "    %s [label=<%s>];\n" % (org_node, org_label)
                    fhw.write(label)

                if next_label:
                    label = "    %s [label=<%s>];\n" % (next_node, next_label)
                    fhw.write(label)



    fh.close()

    fhw.write("}\n")

    fhw.close()
    if log:
        log.info("- created dot file: %s", dot_file)

    return dot_file

'''
simple replacements
'''
def flow_replace(my_str):

    new_str = my_str.replace("<br>", "<br/>").replace("<smf>", "<font point-size=\"10\">").replace("</f>", "</font>")

    return new_str

'''
Return special exit codes from bbtools log files
- bbtools uses java which detects out of memory issues itself rather than the cluster
- using exit codes less than 128 because cori reports "2" if the exit code is 128, 130 ...

'''
def bbtools_log_check(log_file):
    bb_exit_code = 0

    if os.path.isfile(log_file):
        fh = open(log_file, "r")
        for line in fh:
            if "java.lang.OutOfMemoryError" in line:
                bb_exit_code = 42 # HHGTTG - the ultimate answer
            if "java.lang.AssertionError" in line:
                if bb_exit_code == 0:
                    bb_exit_code = 0 # java.lang.AssertionError: To many '=' signs: name=Zymomonas mobilis subsp. mobilis ZM4 = ATCC 31821
                    #bb_exit_code = 40
            if "FAILED to lookup docker image" in line:
                bb_exit_code = 44
            if "command not found" in line:
                bb_exit_code = 45

        fh.close()

    return bb_exit_code

'''
Combine the output_path and path_name into output_path/path_name
- create if it doesn't exist
- copied from micro_lib.py - should replace in sag/*.py code
'''
def get_the_path(path_name, output_path):

    the_path = os.path.join(output_path, path_name)
    if not os.path.isdir(the_path):
        os.makedirs(the_path)
    return the_path

'''
Extract version from the path
module load bbtools;bbversion.sh
37.90
bbversion.sh
'''
def get_bbtools_version(rqc_stats_log, output_path, log):
    log.info("get_bbtools_version")

    bbtools_log = os.path.join(output_path, "bbtools.txt")

    cmd = "bbversion.sh > %s 2>&1"  % (bbtools_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)


    bbtools_version = None
    fh = open(bbtools_log, "r")
    for line in fh:
        bbtools_version = line.strip()

    fh.close()

    log.info("- bbtools version: %s", bbtools_version)
    append_rqc_stats(rqc_stats_log, "bbtools_version", bbtools_version)

    return bbtools_version


'''
Return number of processors available to us
'''
def get_nproc():
    nproc = 16

    # slurm env vars
    #SLURM_CPUS_PER_TASK=16
    #SLURM_CPUS_ON_NODE=16
    #SLURM_JOB_CPUS_PER_NODE=16
    #Actually, it turns out nproc returns the number of processors you are allowed to use in SLURM, so you don't need to parse anything.
    cmd = "nproc"
    std_out, std_err, exit_code = run_cmd(cmd)
    nproc = int(std_out.strip())

    return nproc

'''
Simple web page getter, returns as json if possible
- used in rqc_report.py. validate_fq.py
- be careful, some urls need headers added for authentication
https://2.python-requests.org/en/master/user/quickstart/
'''
def get_web_page(my_url, my_api, log=None):
    timeout_sec = 300
    retry_cnt = 3
    attempt_cnt  = 1
    sleep_sec = 10 # wait this many seconds before retying again

    if not my_url.endswith("/") and not my_api.startswith("/"):
        my_url += "/"
    if my_url.endswith("/") and my_api.startswith("/"):
        my_url = my_url[:-1] # chop off last /

    uri = "%s%s" % (my_url, my_api)

    response = None
    done = False
    msg = ""

    while not done:
        err = False

        try:
            if log:
                log.info("- uri: %s", uri)
            else:
                print ("- uri: %s" % uri)

            r = requests.get(uri, timeout=timeout_sec)
            response = r.text
            msg = "- status_code: %s, received %s bytes" % (r.status_code, len(response))

            done = True

            if r.status_code == 503 and "rqc" in my_url:
                err = True
                msg = "RQC website not up"

        except requests.exceptions.RequestException as e:
            err = True
            msg = "GET request to %s failed: %s" % (uri, e)

        except requests.exceptions.Timeout:
            err = True
            msg = "GET request to %s failed: timed out after %s seconds" % (uri, timeout_sec)

        except requests.exceptions.TooManyRedirects:
            err = True
            msg = "GET request to %s failed: too many redirects" % uri

        except:
            # if unknown reason happens, test by commenting this exception out to see the real reason
            err = True
            msg = "GET request to %s failed: unknown reason" % uri



        if msg:
            if log:
                if err:
                    log.error(msg)
                else:
                    log.info(msg)
            else:
                if err:
                    print ("- error: %s" % msg)
                else:
                    print(msg)

        if err:
            attempt_cnt += 1
            done = False

        if attempt_cnt > retry_cnt:
            done = True

        else:
            if not done:
                if log:
                    log.info("- retrying in %s seconds", sleep_sec)
                else:
                    print ("- retrying in %s seconds" % sleep_sec)

                time.sleep(sleep_sec)


    if response:
        try:
            response = json.loads(response)
        except:
            if log:
                log.info("- not json data (%s)", type(response))
            else:
                print ("- not json data (%s)" % type(response))


    return response


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## main program


if __name__ == "__main__":
    # unit tests
    
    print(get_analysis_project_id(1214774, 1, 47, ".", "", "-sa") )
    #print get_analysis_project_id()
    
    #print get_web_page("https://rqc.jgi-psf.org", "/api/rqcws/dump/12733.1.281029.TCTAAGGCACG.fastq.gz")
    #sys.exit(44)

    #print human_size(51722109061120)
    #print human_size(74259685519360) # 67.5TB
    #print human_size(250000000000)
    #print human_size(  83964563477532.00  )
    #print human_size(155846182240)

    #run_cmd("shifter --image=bryce911/bbtools2 bbversion.sh > /global/projectb/scratch/brycef/read/CWZZU2/bbtools.txt 2>&1")
    #run_cmd("shifter --image=bryce911/bbtools2 bbversion.sh > /global/projectb/scratch/brycef/read/CWZZU2/bbtools.txt")
    #sys.exit(44)

    #print get_read_count_fastq("/global/projectb/scratch/brycef/sag/phix/11185.1.195330.UNKNOWN_matched.fastq.gz")

    #cmd = "cd /tmp && #bbtools;bbduk.sh in=/global/dna/dm_archive/sdm/illumina//01/14/88/11488.1.208132.UNKNOWN.fastq.gz ref=/global/dna/shared/rqc/ref_databases/qaqc/databases/phix174_ill.ref.fa outm=/global/projectb/scratch/brycef/phix/11488/11488.1.208132.UNKNOWN_matched.fastq.gz outu=/global/projectb/scratch/brycef/phix/11488/11488.1.208132.UNKNOWN_unmatched.fastq.gz && #spades/3.12.0;spades.py -v"
    cmd = "cd /tmp && bbduk.sh in=/global/dna/dm_archive/sdm/illumina//01/14/88/11488.1.208132.UNKNOWN.fastq.gz ref=/global/dna/shared/rqc/ref_databases/qaqc/databases/phix174_ill.ref.fa outm=/global/projectb/scratch/brycef/phix/11488/11488.1.208132.UNKNOWN_matched.fastq.gz outu=/global/projectb/scratch/brycef/phix/11488/11488.1.208132.UNKNOWN_unmatched.fastq.gz && spades.py -v"

    cmd = "#R/rocker;sleep 3"

    cmd = "#pigz;pigz /global/projectb/scratch/brycef/align/BTOYH/genome/11463.6.208000.CAAGGTC-AGACCTT.filter-RNA.fastq.gz-genome.sam"

    cmd = "#java;java -version"

    cmd = "#diamond;diamond blastp"

    #dot_flow("/global/projectb/scratch/brycef/pbmid/BWOAU/f2.flow", "/global/projectb/scratch/brycef/pbmid/BWOAU/BWOUAx.dot")

    sys.exit(0)
