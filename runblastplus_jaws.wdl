import "runblastplus_sub_blastn.wdl" as blastplus_sub
workflow runblastplus_main {
    meta {
        version: '1.0.0' 
        author: 'Stephan Trong,Jeff Froula' 
    }
   
    # from inputs.json:
    File queryFile
    Map[String, String] refDb
    Map[String, String] refOutputName
    String params
    Int chunk_size


    # For each ref file defined in the inputs json, blast query with ref in parallel.
    scatter (pair in refDb) {
        call validateBlastDb {
            input: dbpath=pair.right
        }
        call blastplus_sub.runblastplus_blastn_wf{
            input: blast_options=params,
                   queryFile=queryFile,
                   refOutputName=refOutputName[pair.left],
                   refDb=pair.right,
                   chunk_size=chunk_size
        }

        call taxon_processing{
            input: query=queryFile,
                   params=params,
                   blastout=runblastplus_blastn_wf.blastout,
                   prefix=refOutputName[pair.left],
                   refDb=pair.right
        }
    }
}

### ------------------------------ ###

task validateBlastDb {
    String dbpath

    command {
        python <<CODE
        import os, sys, glob
        # refDb path should include prefix "nt" (i.e. /global/dna/shared/nt/nt NOT /global/dna/shared/nt)
        db_dir = os.path.dirname('${dbpath}')
        db_prefix = os.path.basename('${dbpath}')
        if not os.path.exists(db_dir):
             sys.exit("Error: Database directory doesn't exist: %s"%db_dir)

        # does the database already exist?
        if not glob.glob(db_dir + '/' + db_prefix + '*.nin'):
             sys.exit("Error: no index files (i.e. *.nin) in database directory: %s"%'${dbpath}')

        CODE
    }

    runtime {
        docker: "jfroula/jaws-blastplus:1.1.1"
        poolname: "jfsma"
        nodes: 1
        nwpn: 4
        mem: "5G"
        time: "00:30:00"
    }

}

task taxon_processing{
    File query
    File blastout
    String prefix
    String refDb
    String params

    command {
        run_blastplus_postprocessing.py -q ${query} -bo ${blastout} -d ${refDb} -o "./${prefix}"
    }

    runtime {
        docker: "jfroula/jaws-blastplus:1.1.1"
        poolname: "jfsma"
        nodes: 1
        nwpn: 4
        mem: "5G"
        time: "00:30:00"
    }

    output {
        File taxa_files = "${prefix}"
    }
}
