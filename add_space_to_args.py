import re
import sys
mo="--blast-options \"-num_threads 16 -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles'\""
new_params=''
try:
    if re.match(r'-p\s*[\"\'](.*)',mo):
        m=re.match(r'-p\s*[\"\'](.*)',mo).group(1)
    elif re.match(r'--blast-options\s*[\"\'](.*)',mo):
        m=re.match(r'--blast-options\s*[\"\'](.*)',mo).group(1)
    #print("M: %s"%m)
    # add space to begining of args
    new_params = ' ' + m
except:
    sys.exit("No match found")


print("M:%s"%new_params)

        

