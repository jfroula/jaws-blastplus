workflow runblastplus_blastn_wf {
    meta {
        version: '1.0.0'
        author: 'Stephan Trong, Jeff Froula'
    }

    ### from inputs.json:
    File queryFile
    String refDb
    String refOutputName
    Int chunk_size
    String blast_options
    
    call shard {
        input: query = queryFile,
        chunk_size=chunk_size
    }

    Array[String] shard_array = shard.shards

    # For each ref file defined in the inputs json, blast query with ref in parallel.
    #
    scatter(coords in shard_array) {
        call runBlast{
            input: query=queryFile,
                   prefix=refOutputName,
                   refDb=refDb,
                   coords=coords,
                   blast_options=blast_options
       }
    }

    call mergeBlast{
        input: blastout_array=runBlast.blastout,
               prefix=refOutputName
    }

    output {
       File blastout = mergeBlast.merged
    }
}

### ------------------------------------ ###
task shard {
    File query
    String bname = basename(query)
    Int chunk_size

    command {
        set -e -o pipefail
    
       # shifter --image=jfroula/jaws-blastplus:1.1.1 \
		fasta_indexer.py --input ${query} --output ${bname}.index
       # shifter --image=jfroula/jaws-blastplus:1.1.1 \
		create_blocks.py -if ${bname}.index -ff ${query} -of ${bname}.sharded -bs ${chunk_size}
	}

    output {
        Array[String] shards = read_lines("${bname}.sharded")
    }

	runtime {
        docker: "jfroula/jaws-blastplus:1.1.1"
        poolname: "jfsma"
        nodes: 1
        nwpn: 4
        mem: "5G"
        time: "00:30:00"
    }
}

task runBlast {
    File query
    String prefix
    String refDb
    String coords
    String blast_options

    command <<< 
        start=$(echo ${coords} | awk '{print $1}')
        end=$(echo ${coords} | awk '{print $2}')

        # TODO jfroula: we need to add a preceding space to blast_options because 
        # a bug in argparse won't recognize arguments begining with "-" (i.e. -p "-num_threads 8")
		# right now a space has to be provided in the inputs.json

        # we are piping a block of the fastq sequence to the aligner 
       # shifter --image=jfroula/jaws-blastplus:1.1.1 \
		shard_reader.py -i ${query} -s $start -e $end | run_blast.py -db ${refDb} \
          --query_seq -  \
          --query_file ${query}  \
          --output-name ${prefix} ${blast_options}
    >>>

	runtime {
        docker: "jfroula/jaws-blastplus:1.1.1"
        poolname: "jfsma"
        nodes: 1
        nwpn: 4
        mem: "5G"
        time: "00:30:00"
    }

    output {
        File blastout = "${prefix}.parsed"
    }

}

task mergeBlast{
    Array[File] blastout_array
    String prefix
    command {
        # merge and sort by evalue
        cat ${sep=' ' blastout_array} | sort -k4,4 > ${prefix}.merged.fa
    }

    output{
        File merged = "${prefix}.merged.fa"
    }
}
