#!/usr/bin/env python
# -*- coding: utf-8 -*-


'''
run_blastplus: a wrapper for NCBI Blast+ blastn
BLAST = Basic Local Alignment Search Tool
ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/

Created: Feb 3 2016
sulsj (ssul@lbl.gov)


Description
= Purpose: to replace Jigsaw's run_blast.pl
= Localize Blast database files
  - check /scratch/blastdb/global/dna/shared/rqc/ref_databases/ncbi/CURRENT first
= Default blast options
  - can be overridden by user-specified options

blastn –db nt –query QUERY –taxidlist 91347.txids –outfmt 7 –out OUTPUT.ta - limit search by taxids in 91347.txids file
blastn –db nt –query QUERY –taxids 9606 –outfmt 7 –out OUTPUT.ta - species level (human) search


To Do:
- health check for localized dbs
- anything we can localize on Cori?

Dependencies
- NCBI tax db files (nodes.dmp, names.dmp, and gi_taxid_nucl.dmp.gz) for creating lineage info
- BBTools for subsampling, taxonomy server
- blast+

Revisions
    02032016 0.0.9: Created
    02102016 0.1.0: Changed gi2taxid from dict pickled to sqlite for possible memory issue

    02172016 1.0.0: Tested
    02182016 1.1.0: PyCogent tested

    02242016 1.2.0: Reverted to sqlite from pycogent b/c pycogent is not supported on some Genepool compute nodes
    02252016 1.2.1: Updated Blast output sorting criteria
    04142016 1.2.2: Fixed tophit sort params

    04222016 1 3.0: Added outfmt 6,7,10 support
    04282016 1.3.1: Added organism name to subject field (-s option)
    05142016 1.3.2: Fixed organism name adding part (lineage checking)
    06032016 1.3.3: Removed creating symlink from localize_dir; Skip localization if the target dir exists already.
    06032016 1.3.4: Added header line to blast output (.parsed)
    07062016 1.3.5: Removed get_organism_name; Added full lineage infoto the subject field in parsed, tophit, and top100hit instead of species name.

    07082016 1.4.0: Changed blast options: num_alignments=100, num_threads=32
    07112016 1.5.0: Added salltitles for LSSURef, LSURef, SSURef, Collab16s, JGIContaminants which do not have gi
    07192016 1.5.1: Set the default num_threads back to 16

    07212016 1.6.0: Added non-gi subject title support; Updated taxlist generation to handle non-gi subject title; Added nr, refseq.fungi, refseq.mitochondrion, refseq.plant, refseq.plasmid, refseq.plastid, refseq.viral
    07262016 1.6.1: Set num_alignments=5 for blastn

    08102016 1.6.2: Minor bug fixes
    08232016 1.6.3: Fixed taxlist file creation for custom reference db

    08312016 1.7.0: Rewrote localize_dir2()
    09152016 1.7.1: Updated to cope with the case when gi2taxid fails (RQCSUPPORT-813)
    09162016 1.7.2: Removed redundant gi info from salltitles before adding lineage info to subject field
    09232016 1.7.3: Replaced ':' and ';' with '_' in the subject string
    10212016 1.7.4: Added user specified output file name
    11212016 1.7.5: Changed blastn version to 2.5.0
    12122016 1.7.6: Changed default taxonomy dump location

    12192016 2.0.0: Updated to use tax server
    01182017 2.0.1: Set to taxonomy server at taxonomy.jgi-psf.org

    05152017 2.1.0: Used both gi and accession number
    05242017 2.1.1: Updated to try to search header string from the taxonomy server if there is no gi or accession

    08022017 2.2.0: Enabled burst buffer on cori
    08032017 2.2.1: Added "-f" for fungal pipeline (Append salltitles to subject field and skip collecting lineage info)

    08072017 2.3.0: Updated to use persistent burst buffer on Cori
    08072017 2.3.1: Updated to compute and add pctQuery in taxlist output
    08152017 2.3.2: Fixed a bug in addSalltitles
    08232017 2.3.3: Updated to try to search taxonomy server with fasta header if there is no gi or accession
    08232017 2.3.4: Updated to cope with header string starting with number in get_taxa_by_gi_or_acc_or_tid()
    10132017 2.3.5: Upgraded blast+ to 2.7.0 on denovo and cori
    10232017 2.3.6: Changed the delimiter from space to tab in tophit
    10262017 2.3.7: Added green-genes, collab16s, ssu, lsu and others to db options
    11152017 2.3.8: Removed skipping db localization on Denovo
    01242018 2.3.9: Changed tophit output to tab delimited
    01292018 2.3.10: Reverted back to Blast+ 2.6 (Requested by Jasmyn and Alex S)

    02272018 2.4.0: Updated get_taxa_by_gi_or_acc_or_tid for tid
    03292018 2.4.1: Fixed taxlist file creation for refseq.bacteria (RQC-1100)
    04122018 2.4.2: Fixed haeders incuding "gb|" for taxonomy server search (RQCSUPPORT-2366)
    11122018 2.4.3: Wrapped taxonomy curl call in a retry and wait loop

    11132018 2.5.0: refactored to use RQC standards
    11152018 2.5.1: fix for Fungal using 2kb control db (RQCSUPPORT-3161)
    11192018 2.5.2: fix for Fungal for 2 outputs (mito & main) (RQCSUPPORT-3161)
    11282018 2.5.3: fix for Bill not using full path (RQCSUPPORT-3161)
    01242019 2.5.4: fix for "," in the tax_id string (RQCSUPPORT-3297)
    20190415 2.6.0: do not return taxonomy for gi lookups, disabled localization (RQC-1255)
                  : handle refseq_bacteria filenames from NCBI (RQCSUPPORT-3431)
    20190416 2.6.1: fix for condition with taxonomy in header from UNITE (RQC-1255)
    20190418 2.6.2: fix for full pathname for refseq (RQC-1255)
             2.6.3: fix for -1 for tid (RQC-1266, RQCSUPPORT-3553)
    20190423 2.6.4: fix for -1 in the taxonomydict (RQC-1255)
    20190424 2.6.5: fix for creating *.parsed.taxlist from ITS sequence (RQC-1255)
    20190502 2.6.6: fix for missing tax_id (no lineage returned from tax server) (RQC-1255)
    20190516 2.6.7: fix for bad variable (lineage)
    20190905 2.6.8: localize to cscratch if one or more *.nhr files are there (RQC-1312)
    
    
    
Test: GAUPP - 500 reads, runs in seconds on denovo node with localized blast db
~/git/jgi-rqc-pipeline/tools/run_blastplus.py -o t1 -pl

T2: GAOGO - microbial isolate - 246167
~/git/jgi-rqc-pipeline/tools/run_blastplus.py -o t2 -pl

# fungal test - runs in seconds
~/git/jgi-rqc-pipeline/tools/run_blastplus.py \
--db /global/dna/shared/rqc/ref_databases/qaqc/databases/2kb_control.fasta \
-q /global/projectb/sandbox/rqc/analysis/qc_user/blast/fungal_test.fasta.orig \
-c megablast.polished_assembly.fasta.vs.2k-control \
-o /global/projectb/scratch/brycef/blast/fungal -f -s -n \
-p='-task megablast -perc_identity 90 -evalue 1e-30 -dust "yes" -num_threads 8' -pl

~/git/jgi-rqc-pipeline/tools/run_blastplus.py \
--db /global/dna/shared/rqc/ref_databases/qaqc/databases/2kb_control.fasta \
-q /global/projectb/sandbox/rqc/analysis/qc_user/blast/fungal_test.fasta.orig \
-c megablast.polished_assembly.fasta.vs.2k-control \
-f -s -n \
-p='-task megablast -perc_identity 90 -evalue 1e-30 -dust "yes" -num_threads 8' -pl


# new shortened command
~/git/jgi-rqc-pipeline/tools/run_blastplus.py \
--db 2kb-control \
-q /global/projectb/sandbox/rqc/analysis/qc_user/blast/fungal_test.fasta.orig \
-c megablast.polished_assembly.fasta.vs.2k-control \
-o /global/projectb/scratch/brycef/blast/fungal-new --fungal -pl


~/git/jgi-rqc-pipeline/tools/run_blastplus.py \
--db gcontam-2kb \
-q /global/projectb/scratch/brycef/blast/BTOOT.fasta \
-c megablast.mito.vs.gcontam \
-o /global/projectb/scratch/brycef/blast/BTOOT2 \
-f -s -n -p='-task megablast -evalue 1e-10' -pl


RQCSUPPORT-3161 - Jasmyn support - same output folder, 2 different custom names
~/git/jgi-rqc-pipeline/tools/run_blastplus.py \
--skip-localization \
--db /global/dna/shared/rqc/ref_databases/ncbi/gcontam+2kb_control/gcontam+2kb_control \
-q /global/projectb/scratch/jasmynp/fungal_std/GAA-3934/arrow/polished_assembly.fasta \
-c megablast.main.vs.gcontam \
-o /global/projectb/scratch/brycef/blast/BTOOT2 \
-f -s -n -p='-task megablast -evalue 1e-10'

~/git/jgi-rqc-pipeline/tools/run_blastplus.py \
--skip-localization \
--db /global/dna/shared/rqc/ref_databases/ncbi/gcontam+2kb_control/gcontam+2kb_control \
-q /global/projectb/scratch/jasmynp/fungal_std/GAA-3934/mito_arrow/polished_assembly.fasta \
-c megablast.mito.vs.gcontam \
-o /global/projectb/scratch/brycef/blast/BTOOT2 \
-f -s -n -p='-task megablast -evalue 1e-10'

Bill condition
cd /global/projectb/scratch/andreopo/GAA-3956/new_run/its/ITS_assembleThisPacBio
cd /global/projectb/scratch/brycef/blast/bill
~/git/jgi-rqc-pipeline/tools/run_blastplus.py -pl \
--skip-localization --db ../its.fasta \
-q ./CA_bbd005/9-terminator/asm.scf.fasta \
-c megablast.asm2.vs.its -o . -f -s -n \
-p='-task megablast -perc_identity 90 -evalue 1e-30 -dust "yes" -num_threads 8'

RQCSUPPORT-3297
~/git/jgi-rqc-pipeline/tools/run_blastplus.py -s \
-o /global/projectb/scratch/brycef/blast/GBTYC \
-q $RQC_SHARED/pipelines/microbe_iso/archive/03/02/60/95/trim/scaffolds.trim.fasta -d collab16s

RQCSUPPORT-3389
~/git/jgi-rqc-pipeline/tools/run_blastplus.py \
--db /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nt/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted \
-q /global/cscratch1/sd/brycef/blast/polished_assembly.fasta \
-c megablast.polished_assembly.fasta.vs.nt \
-o /global/cscratch1/sd/brycef/blast/3389 \
-f -s -n -p='-task megablast -perc_identity 90 -evalue 1e-30 -dust "yes" -num_threads 8' -pl

/global/cscratch1/sd/brycef/blast

> /global/cscratch1/sd/jasmynp/fungal_std/GAA-3806/20190213/blast/megablast.polished_assembly.fasta.vs.nt.log 2>&1

/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/run_blastplus_taxserver.py  --db /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/refseq.bacteria/refseq.bacteria -q /global/cscratch1/sd/jasmynp/fungal_std/GAA-3806/20190213/arrow/polished_assembly.fasta -c megablast.polished_assembly.fasta.vs.refseq.bacteri -o /global/cscratch1/sd/jasmynp/fungal_std/GAA-3806/20190213/blast -f -s -n -p='-task megablast -perc_identity 90 -evalue 1e-30 -dust "yes" -num_threads 8'



~/git/jgi-rqc-pipeline/tools/run_blastplus_taxserver.py  --db /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/refseq.mitochondrion/refseq.mitochondrion -q polished_assembly.fasta -c megablast.polished_assembly.fasta.vs.refseq.mito -f -s -n -p='-task megablast -perc_identity 90 -evalue 1e-30 -dust "yes" -num_threads 8'
~/git/jgi-rqc-pipeline/tools/run_blastplus.py \
--db /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/refseq.mitochondrion/refseq.mitochondrion \
-q polished_assembly.fasta \
-c megablast.polished_assembly.fasta.vs.refseq.mito -f -s -n \
-p='-task megablast -perc_identity 90 -evalue 1e-30 -dust "yes" -num_threads 8'

/global/cscratch1/sd/jasmynp/fungal_std/rqc_tests/bbtools_38.44/GCPNW/blast/megablast.polished_assembly.fasta.vs.refseq.archaea.parsed

~/git/jgi-rqc-pipeline/tools/run_blastplus.py --db unite -f -s -n -p='-task megablast -perc_identity 90 -evalue 1e-30 -dust "yes" -num_threads 8' -q ITS.full.fasta -o /global/projectb/scratch/brycef/blast/ITS

~/git/jgi-rqc-pipeline/tools/run_blastplus.py -s \
-o /global/projectb/scratch/brycef/blast/GHOSG \
-q /global/dna/shared/rqc/pipelines/microbe_iso/archive/03/03/35/23/trim/scaffolds.trim.fasta \
-d nt -pl

To do:
~/git/jgi-rqc-pipeline/tools/run_blastplus.py \
--db /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nt/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted \
-q /global/projectb/scratch/klabutti/fungal_std/GAA-4129.Erysiphe_pisi_Palampur-1.GBBPA/arrow/polished_assembly.fasta \
-c megablast.polished_assembly.fasta.vs.nt \
-o /global/projectb/scratch/brycef/blast/GBBPA \
-f -s -n -p='-task megablast -perc_identity 90 -evalue 1e-30 -dust "yes" -num_threads 8' -pl

ITS Test:
~/git/jgi-rqc-pipeline/tools/run_blastplus.py \
--db /global/cscratch1/sd/jasmynp/fungal_std/rqc_tests/bbtools_38.44/GCPNW/its/its.fasta \
-q /global/cscratch1/sd/jasmynp/fungal_std/rqc_tests/bbtools_38.44/GCPNW/arrow/polished_assembly.fasta \
-c megablast.asm.vs.its -f -s -n \
-p='-task megablast -perc_identity 90 -evalue 1e-30 -dust "yes" -num_threads 8'  \
-o /global/projectb/scratch/brycef/blast/GCPNW -pl

'''

import os
import sys
import argparse
import re ## for pctQuery
import time
import getpass
import glob
import json

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)


#from common import get_logger, get_colors, run_cmd, convert_cmd, get_msg_settings
from common import get_logger, get_colors, run_cmd, get_msg_settings
from rqc_constants import RQCReferenceDatabases
from taxonomy import get_taxonomy, format_taxonomy

## Globals
VERSION = "2.6.8"
SCRIPT_NAME = __file__

NERSC_HOST = os.environ.get('NERSC_HOST', 'unknown') # not used anymore, was used to determine localization

BLAST_VERSION = "blast+/2.6.0"
#BLAST_VERSION = "blast+/2.7.0"

## Outfmt
##
## ex)
## MISEQ04:278:000000000-ALA09:1:1101:5246:13068    gi|851289456|ref|NZ_AXDV01000017.1| 279 9e-74   151 100.00  1   151 151 3509    3659    5516    plus    N/A
## MISEQ04:278:000000000-ALA09:1:1101:5638:14198    gi|851289456|ref|NZ_AXDV01000017.1| 279 9e-74   151 100.00  1   151 151 2881    3031    5516    plus    N/A
##
## ex2)
## MISEQ04:278:000000000-ALA09:1:1101:15574:1382    gi|320006635|gb|CP002475.1| 274 3e-70   151 99.34   1   151 151 4576476 4576626 7337497 591167
## MISEQ04:278:000000000-ALA09:1:1101:15574:1382    gi|478743931|gb|CP003990.1| 268 1e-68   151 98.68   1   151 151 2895762 2895612 7526197 1265601
##
## ex3) with salltitles
## LSSURef, LSURef, SSURef, Collab16s, JGIContaminants do not have gis. Instead, salltitles will contain the lineage info.
##
## vs. LSSURef
## NODE_1_length_1097_cov_5.158615  AHJR01000001.1122280.1125317    2108    0.0 1159    99.482  11159   1159    1716    558 3038    N/A AHJR01000001.1122280.1125317_Archaea;Crenarchaeota;Thermoprotei;Sulfolobales;Sulfolobaceae;Sulfolobus;Sulfolobus_islandicus_M.16.43
##
## vs. collab16s
## NODE_10_length_278_cov_2.492806  38238_Sulfolobus_solfataricus_98/2_W2_resequencing_LIBS_OYPP_POOLS_OPXT 440 2.26e-124   238 100.000 103 340 340 1499    1262    1499    N/A 38238_Sulfolobus_solfataricus_98/2_W2_resequencing_LIBS_OYPP_POOLS_OPXT
##
## New blast+ options for taxonomy info
## Ref) http://blastedbio.blogspot.com/2012/05/blast-tabular-missing-descriptions.html
## staxids sscinames scomnames sblastnames sskingdom
## - staxids means unique Subject Taxonomy ID(s), separated by a ';' (in numerical order)
## - sskingdoms means unique Subject Super Kingdom(s), separated by a ';' (in alphabetical order)
##
## Ref) https://www.biostars.org/p/76551/
## taxdb (ftp://ftp.ncbi.nlm.nih.gov/blast/db/taxdb.tar.gz) should be found from BLASTDB
##
## Ref) http://www.verdantforce.com/2014/12/building-blast-databases-with-taxonomy.html
##
## This works only for nt
## $ export BLAST DB=$BLASTDB:/global/projectb/scratch/sulsj/2016.02.03-run_blastn_py-test/
## $ blastn -query 10184.1.150307.ATGTC.fasta -db /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nt/nt -num_threads 8 -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids'
##
## Added stitle
## Ref) http://blastedbio.blogspot.com/2012/05/blast-tabular-missing-descriptions.html
## Removed qstrand and sstrandq
##
DEFAULT_OUTFMT = " -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles' "
DEFAULT_BLAST_OPTIONS = " -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true -num_alignments 5 %s " % (DEFAULT_OUTFMT)
FUNGAL_BLAST_OPTIONS = " -task megablast -perc_identity 90 -evalue 1e-30 -dust \"yes\" -num_threads 8 %s " % (DEFAULT_OUTFMT)

"""
Run blastn and return blast output in tabular format
* blastn only works with fasta files, run_blastplus will convert fastq to fasta
"""
def create_blastn_cmd(queryFile, dbFile, blastOptions, customOutputName):
    log.info("create_blastn_cmd: %s, %s", queryFile, dbFile)

    fastaFileBasename = os.path.basename(queryFile)
    dbFileBasename = os.path.basename(dbFile)
    if dbFileBasename.startswith("refseq_"):
        dbFileBasename = dbFileBasename.replace("refseq_", "refseq.") # RQCSUPPORT-3531, keep blastOutFile the same so pipelines don't break

    exitCode = 0

    if customOutputName:
        blastOutFile = os.path.join(outputPath, customOutputName + ".parsed")
    else:
        blastOutFile = os.path.join(outputPath, "megablast." + fastaFileBasename + ".vs." + dbFileBasename + ".parsed")
    
    done_file = os.path.join(outputPath, "run_blastplus-%s.%s.done" % (fastaFileBasename, os.path.basename(blast_db)))
    if blast_db.endswith(".fasta"):
        done_file = os.path.join(outputPath, "run_blastplus-%s.%s.done" % (fastaFileBasename, os.path.basename(blast_db)))

    if customOutputName:
        done_file = os.path.join(outputPath, customOutputName + ".done")


    if os.path.isfile(done_file):
        log.info("- skipping blast run, found file: %s", done_file)
    else:
        blastnCmd = "%s;" % (BLAST_VERSION)
        blast_log = blastOutFile + ".log"

        if queryFile.endswith(".gz"):
            cmd = "zcat %s | %s blastn -db %s %s > %s 2> %s" % (queryFile, blastnCmd, dbFile, blastOptions, blastOutFile, blast_log)
        else:
            cmd = "%s blastn -query %s -db %s %s > %s 2> %s" % (blastnCmd, queryFile, dbFile, blastOptions, blastOutFile, blast_log)

    # create json file of all blastn parameters
    # blastn_params = {"queryFile": queryFile, 
    #                  "blastnCmd": blastnCmd,
    #                  "dbFile": dbFile,
    #                  "blastOptions": blastOptions,
    #                  "blastOutFile": blastOutFile,
    #                  "blast_log": blast_log}

    with open('blast.cmd', 'w') as fp:
        fp.write(cmd)


            #stdOut, stdErr, exitCode = run_cmd(cmd, log)
#bbtools
        # it will exit with exit code = 0 but still be an error
        #log.info("- checking log: %s", blast_log)
#        fh = open(blast_log, "r")
#        for line in fh:
#            if line.startswith("Error: NCBI C++ Exception"):
#                log.error("- NCBI Blast Error!  %s", msg_fail)
#                sys.exit(1)
#                # this error occurs when the query is not fasta format (e.g. fastq)
#            if "FAILED to" in line:
#                log.error("- SHIFTER error!  %s", msg_fail)
#                # happened during maintenance, shifter not available
#                sys.exit(1)
#            if line.startswith("BLAST Database error"):
#                log.error("- BLAST db error!  %s", msg_fail)
#                sys.exit(1)
#
#            if "command not found" in line:
#                log.error("- SHIFTER error!  %s", msg_fail)
#                sys.exit(1)
#        fh.close()
#
#        if exitCode != 0:
#            log.error("- Failed to create "blast.cmd". Unable to run the create_blastn_cmd command! Exit code != 0")
#            sys.exit(1)
#
#        fh = open(done_file, "w")
#        fh.write("# blast for %s vs %s complete\n" % (fastaFileBasename, blast_db))
#        fh.write("# command:\n %s\n" % cmd)
#        fh.close()

#    return blastOutFile


'''
Translate "nt" -> /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nt/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted
All these dbs are stored in lib/rqc_constants.py

'''
def get_db_alias(db_name):

    new_db = ""

    #if db_name.startswith("/") or db_name.startswith("."):
    #    return db_name

    # consider other aliases, e.g. refseq.archaea or archaea
    # if dbFile == "refseq.microbial":
    #     dbFile = RQCReferenceDatabases.REFSEQ_MICROBIAL

    if db_name == "refseq.archaea":
        new_db = RQCReferenceDatabases.REFSEQ_ARCHAEA
    elif db_name == "refseq.bacteria":
        new_db = RQCReferenceDatabases.REFSEQ_BACTERIA
    elif db_name == "nr":
        new_db = RQCReferenceDatabases.NR
    elif db_name == "nt":
        new_db = RQCReferenceDatabases.NT_maskedYindexedN_BB
    elif db_name == "refseq.fungi":
        new_db = RQCReferenceDatabases.REFSEQ_FUNGI
    elif db_name.startswith("refseq.mito"): # refseq.mitochondrion
        new_db = RQCReferenceDatabases.REFSEQ_MITOCHONDRION

    elif db_name == "refseq.plant":
        new_db = RQCReferenceDatabases.REFSEQ_PLANT
    elif db_name == "refseq.plasmid":
        new_db = RQCReferenceDatabases.REFSEQ_PLASMID
    elif db_name == "refseq.plastid":
        new_db = RQCReferenceDatabases.REFSEQ_PLASTID
    elif db_name == "refseq.viral":
        new_db = RQCReferenceDatabases.REFSEQ_VIRAL
    elif db_name == "green_genes": # Should not be used
        new_db = RQCReferenceDatabases.GREEN_GENES
    elif db_name == "lsu":
        new_db = RQCReferenceDatabases.LSU_REF
    elif db_name == "ssu":
        new_db = RQCReferenceDatabases.SSU_REF
    elif db_name == "lssu":
        new_db = RQCReferenceDatabases.LSSU_REF
    elif db_name == "collab16s":
        new_db = RQCReferenceDatabases.COLLAB16S
    elif db_name.startswith("sagtam"):
        new_db = RQCReferenceDatabases.SAGTAMINANTS
    elif db_name == "microbial_watchlist":
        new_db = RQCReferenceDatabases.MICROBIAL_WATCHLIST
    elif db_name.startswith("jgicontam"):
        new_db = RQCReferenceDatabases.CONTAMINANTS
    elif db_name == "control-2kb" or db_name == "2kb-control":
        new_db = RQCReferenceDatabases.CONTROL_2KB
    elif db_name == "control-4kb" or db_name == "4kb-control":
        new_db = RQCReferenceDatabases.CONTROL_4KB
    elif db_name == "gcontam-2kb":
        new_db = RQCReferenceDatabases.GCONTAM_2KB
    elif db_name == "unite":
        new_db = RQCReferenceDatabases.UNITE
    elif db_name == "univec":
        new_db = RQCReferenceDatabases.UNIVEC

    if db_name.startswith("refseq"):
        if not os.path.isfile(new_db):
            # RQCSUPPORT-3531, NCBI changed file name format for refseq
            new_db = os.path.join(os.path.dirname(new_db), os.path.basename(new_db).replace("refseq.", "refseq_"))
    else:
        if "refseq." in os.path.basename(db_name):
            if not os.path.isfile(db_name):
                # RQC-1255 - Jasmyn insists on using the full path instead of -db refseq.mito
                new_db = os.path.join(os.path.dirname(db_name), os.path.basename(db_name).replace("refseq.", "refseq_"))


    if not new_db:
        new_db = db_name


    return new_db

'''
On denovo, the localized db should be /scratch/blastdb/ + global/dna/shared/rqc/rqc_databases/ncbi/CURRENT
Cori = ? - no local disk.  2019-04-15 - skip localization
Localdisk MUCH faster than /global/dna/shared ...
'''
def get_localized_db(db_file):
    log.info("get_localized_db: %s", db_file)

    # 2019-04-10: localization disabled - Denovo down, Cori does not have local disk
    if 1 == 2:
        if db_file.startswith("/"):
            localized_folder = os.path.join("/scratch/blastdb/", db_file[1:])

            if os.path.isdir(os.path.dirname(localized_folder)):
                log.info("- changing to localized folder: %s", localized_folder)
                db_file = localized_folder

                # do a health check

                # could rsync the folder but would take a while
                # currently NERSC handles this for us

    # 2019-09-05 - cscratch location
    # /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nt/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted
    # /global/cscratch1/sd/qc_user/dna/shared/rqc/ref_databases/ncbi/

    new_db_file = db_file.replace("/global/dna", "/global/cscratch1/sd/qc_user/dna")
    test_file = "%s.%s" % (new_db_file, "*.nhr")
    
    nhr_list = glob.glob(test_file)
    if len(nhr_list) > 0:
        
        db_file = new_db_file
        log.info("- new cscratch location: %s  %s", db_file, msg_ok)

    else:
        log.info("- cannot find cscratch mirror  %s", msg_warn)
        
    return db_file

##==============================




#def main():
if __name__ == '__main__':

    desc = """Run Blast+:
Wrapper for running Blast+ for different databases
    """

    tax_list = ['kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']
    tax_list_dict = {} # dict of count of each kingdom|...|species

    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)
    uname = getpass.getuser()

    parser = argparse.ArgumentParser(description=desc)



    parser.add_argument("-c", "--output-name", dest="outputName", help="Custom output file name") # fungal uses this
    parser.add_argument("-d", "-db", "--db", dest="dbFile", required=True, help="Reference Blast db, Full path to BLAST db or \
[refseq.archaea, refseq.bacteria, nt, nr, refseq.fungi, refseq.mitochondrion, refseq.plant, refseq.plasmid, \
refseq.plastid, refseq.viral, ssu, lsu, lssu, collab16s, sagtam, jgicontam, control-2kb, control-4kb]")
    parser.add_argument("-o", "--output-path", dest="outputPath", help="Target path to write output files to")
    parser.add_argument("-q", "--query", dest="queryFile", required=True, help="Input query file (full path to fasta or gzipped fasta)")

    parser.add_argument("-f", "--add-salltitles", action="store_true", dest="addSalltitles", default=False, help="Append salltitles to subject field and skip collecting lineage info")
    #parser.add_argument("-k", "--keep-orig-parsed", action="store_true", dest="keepOrigParsed", default=False, help="Keep original parsed output")
    parser.add_argument("-l", "--skip-localization", action="store_true", dest="skipLocalization", default=False, help="Skip database localization")
    parser.add_argument("-n", "--add-pct-query", action="store_true", dest="addPctQuery", default=False, help="Add pctQuery value in taxlist output")

    parser.add_argument("-p", "--blast-options", dest="blastOptions", help="User-specified Blast+ options. Default: -num_threads 16 -evalue 1e-30 \
                        -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true -outfmt '6 qseqid sseqid bitscore evalue \
                        length pident qstart qend qlen sstart send slen staxids salltitles'. Note: top hits, top 100 hits, and tax list files will be generated \
                        only with outfmt = 6 (non-custom), 7, 10. Ex1) run_blastplus.py -p=\"-perc_identity 70\", Ex2) run_blastplus.py -p=\"-evalue \
                        1e-30 -perc_identity 90 -word_size 45 -outfmt 5\", Ex3) run_blastplus.py -p=\"-evalue 1e-20 -perc_identity 80 \
                        -outfmt '6 qseqid sseqid bitscore evalue pident'\"")

    parser.add_argument("-s", "--add-species", action="store_true", dest="addLineage", default=False, help="Add taxonomy lineage/organism name to subject field")
    parser.add_argument("-t", "--num-threads", dest="numThreads", default=16, help="Number of threads to run (default: 16)")
    # parser.add_argument("-bv", "--blast-version", dest="blast_version", help="Blast version to run, default = %s" % BLAST_VERSION)
    # Fungal is using -f -s and -n
    parser.add_argument("--fungal", dest="fungal_flag", default=False, action="store_true", help="use fungal options (-f -s -n and blast options %s)" % FUNGAL_BLAST_OPTIONS)
    parser.add_argument("-v", "--version", action="version", version=VERSION)
    parser.add_argument("-pl", "--print-log", dest="print_log", default=False, action="store_true", help="print log to screen")

    options = parser.parse_args()
    print_log = options.print_log
    fungal_flag = options.fungal_flag

    do_log = True

    ## Mandatory options
    queryFile = options.queryFile
    blast_db = options.dbFile # keep original
    dbFile = options.dbFile


    skipLocalization = False
    blastOutfmtOption = None
    addLineage = False ## append lineage info to subject
    customOutputName = None
    addSalltitles = False ## append salltitles to subject
    #keepOrigParsed = False ## not used
    addPctQuery = False

    ## Burst Buffer on Cori
    dw_job_bbuffer = None
    dw_persistent_striped_ncbi_db = None

    # if options.blast_version:
    #     bver = options.blast_version
    #     if bver[0].isdigit():
    #         bver = "blast+/%s" % bver


    #     cmd = "#%s;blastn -help" % bver
    #     new_cmd = convert_cmd(cmd)
    #     if new_cmd.startswith("#"):
    #         print "Error: cannot find shifter container for %s  %s" % (options.blast_version, msg_fail)
    #         print "- shifter container needs to be setup in lib/common.py to use this option"
    #         print "- or blast version requested incorrect, needs to be in the format 'blast+/2.x.y'"
    #         sys.exit(2)
    #     else:
    #         BLAST_VERSION = bver


    if options.outputPath:
        outputPath = options.outputPath
    else:
        outputPath = os.getcwd()

    if options.blastOptions:
        if "-outfmt" not in options.blastOptions: ## force to generate tabular output
            options.blastOptions += DEFAULT_OUTFMT
            blastOutfmtOption = "default"

        DEFAULT_BLAST_OPTIONS = options.blastOptions


    if options.numThreads:
        numThreads = options.numThreads

    if "num_threads" not in DEFAULT_BLAST_OPTIONS:
        DEFAULT_BLAST_OPTIONS += " -num_threads %s" % (numThreads)


    addLineage =  options.addLineage
    addSalltitles = options.addSalltitles
    #keepOrigParsed = options.keepOrigParsed
    addPctQuery = options.addPctQuery
    skipLocalization = options.skipLocalization



    # BF 2018-11-12: don't use burst buffer on Cori (NERSC recommendation)
    if 1 == 2:
        if not skipLocalization and NERSC_HOST == "cori": ## check burst buffer on cori
            if "DW_JOB_STRIPED" in os.environ and os.environ['DW_JOB_STRIPED'] is not None:
                dw_job_bbuffer = os.environ['DW_JOB_STRIPED']
            elif "DW_JOB_PRIVATE" in os.environ and os.environ['DW_JOB_PRIVATE'] is not None:
                dw_job_bbuffer = os.environ['DW_JOB_PRIVATE']

            if "DW_PERSISTENT_STRIPED_NCBI_DB" in os.environ and os.environ['DW_PERSISTENT_STRIPED_NCBI_DB'] is not None: ## if persisten bbf found
                skipLocalization = False
                dw_job_bbuffer = None
                dw_persistent_striped_ncbi_db = os.environ['DW_PERSISTENT_STRIPED_NCBI_DB']
            elif dw_job_bbuffer is not None: ## final check for burst buffer
                skipLocalization = False
            else:
                skipLocalization = True



    if options.outputName:
        customOutputName = options.outputName


    if outputPath and outputPath.startswith("t"):
        test_path = "/global/projectb/scratch/%s/blast" % uname
        #print "output = %s" % outputPath
        if outputPath == "t1":
            # 2019-04-11 - runs in 1 minute on exvivo node
            queryFile = "/global/projectb/sandbox/rqc/analysis/qc_user/blast/GAUPP.500.fastq.gz"
            #queryFile = "/global/projectb/sandbox/rqc/analysis/qc_user/blast/GAUPP.500.fastq"
            # small test of 500 reads
            blast_db = "nt"
            #blast_db = "refseq.archaea"
            #blast_db = "refseq.viral"

            # GAUPP, tax_id = 80863 = "Populus tremula x Populus alba"
            #reformat.sh samplereadstarget=250 in=/global/dna/dm_archive/sdm/illumina//01/27/34/12734.4.280691.ACCACGAT-ATCGTGGT.fastq.gz out=GAUPP.500.fastq.gz ow=t
            outputPath = "GAUPP"
            # md5sum 8ead07667b67b97635131d88a496088d  nt.tax_count.txt

        if outputPath == "t2":
            # isolate assembly
            queryFile = "/global/projectb/sandbox/rqc/analysis/qc_user/blast/GAOGO.fasta"
            # 246167 = 	Vibrio crassostreae
            outputPath = "GAOGO"
            blast_db = "nt"
            #queryFile = "/global/projectb/scratch/brycef/blast/GHHNC.fa"
            #outputPath = "GHHNC-B"

        if outputPath == "t3":
            queryFile = "/global/projectb/sandbox/rqc/analysis/qc_user/blast/GHBGZ.fasta"
            outputPath = "GHBGZ"
            blast_db = "nt"

        if options.dbFile:
            blast_db = options.dbFile

        outputPath = os.path.join(test_path, outputPath)
        dbFile = blast_db

    if fungal_flag:
        addPctQuery = True
        addSalltitles = True
        addLineage = True
        DEFAULT_BLAST_OPTIONS = FUNGAL_BLAST_OPTIONS


    dbFile = get_db_alias(dbFile)

    # testing for new version
    #if uname == "brycef":
    #    dbFile = dbFile.replace("CURRENT", "20190408")


    # use current directory if no output path
    if not outputPath:
        outputPath = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(outputPath):
        os.makedirs(outputPath)


    logFileName = "run_blastplus.log"
    log = get_logger("run_blast", os.path.join(outputPath, logFileName), "INFO", print_log, True)

    log.info("%s", 80 * "~")
    log.info("Starting %s %s", SCRIPT_NAME, VERSION)

    log.info("")

    log.info("Run settings:")
    # these are the only options used by RQC
    log.info("%25s      %s", "blast version", BLAST_VERSION)
    log.info("%25s      %s", "output_path", outputPath)
    log.info("%25s      %s", "input_query", queryFile)
    log.info("%25s      %s", "blast database", blast_db)
    log.info("%25s      %s", "blast database path", dbFile)
    log.info("%25s      %s", "blast options", DEFAULT_BLAST_OPTIONS)
    log.info("%25s      %s", "customOutputName", customOutputName)
    if addPctQuery:
        log.info("%25s      %s", "addPctQuery", addPctQuery)
    if addSalltitles:
        log.info("%25s      %s", "addSalltitles", addSalltitles)
    if addLineage:
        log.info("%25s      %s", "addLineage", addLineage)


    # check if path exists
    if not dbFile:
        log.error("- no db file found!  %s", msg_fail)
        sys.exit(2)


    if not os.path.isfile(queryFile):
        log.error("- no query file: %s  %s", queryFile, msg_fail)
        sys.exit(2)





    ############################################################################
    ## Blast db localization
    ############################################################################

    if not skipLocalization:
        # does this exist on the tmp folder?
        dbFile = get_localized_db(dbFile)

    # wallclock time for logging
    start_time = time.time()



    # convert fastq to fasta
    if queryFile.endswith(".fastq") or queryFile.endswith(".fastq.gz"):
        # .fq, .fq.gz
        log.info("- converting fastq to fasta")
        convert_log = os.path.join(outputPath, "convert.log")
        new_query_file = os.path.basename(queryFile).replace(".fastq", ".fasta")
        new_query_file = os.path.join(outputPath, new_query_file)
        new_query_file = new_query_file.replace(".gz", "") # blastn doesn't work well with zcat | blastn ...
        cmd = "reformat.sh in=%s out=%s ow=t > %s 2>&1" % (queryFile, new_query_file, convert_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        log.info("- using query file: %s", new_query_file)
        queryFile = new_query_file


    ## Get the number of query to compte pctQuery value and set it in taxlist
    totalQuery = 0 ## total number of queries/contigs

    #if addPctQuery:
    if 1 == 1:
        # change to stats.sh?  reformat seems quicker... and it works
        cmd = "reformat.sh %s" % (queryFile)
        stdOut, stdErr, exitCode = run_cmd(cmd, log)
        assert exitCode == 0

        for l in stdErr.split('\n'):
            #Input:                          35 reads                5151078 bases
            if l.startswith("Input:"):
                m = re.findall("(\d+.\d*)", l)
                log.info("- Input query read and base cnt = %s, %s", m[0], m[1])
                totalQuery = int(m[0])
                break


    ############################################################################
    ## Run Blastn
    ############################################################################
    log.info("Blast search options: %s", DEFAULT_BLAST_OPTIONS)

    blastOutputFile = create_blastn_cmd(queryFile, dbFile, DEFAULT_BLAST_OPTIONS, customOutputName)

#    assert os.path.isfile(blastOutputFile), "ERROR: blast output file not found"

