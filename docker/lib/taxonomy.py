#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Library to look up taxonomy ids (ncbi or gi) using BBTools taxonomy server

"""
from __future__ import print_function

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


import os
import sys
import json
import time
import requests

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions


'''
Return taxonomy
'''
def get_taxonomy(my_tax_id, tax_type, log = None):

    tax_dict = {
        "superkingdom" : "-",
        "domain" : "-", # aka kingdom
        "phylum" : "-",
        "class" : "-",
        "order" : "-",
        "family" : "-",
        "genus" : "-",
        "species" : "-"
    }

    if my_tax_id: # > 0:

        # problem with "-" for values
        # Viruses;phylum Viruses;class Viruses;order Viruses;Parvovirinae;Erythroparvovirus;Primate erythroparvovirus 1
        # D,P,C,O,F,G,S
        # Viruses;-;-;-;Parvovirinae;Erythroparvovirus;Primate erythroparvovirus 1
        # Viruses D -> F
        my_tax_id = str(my_tax_id).replace(" ", "_").replace("/", "_").replace(",", "_")
        r = get_taxonomy_service(my_tax_id, tax_type, log)
        
        if not r:
            return tax_dict
        #print my_tax_id
        #print r, type(r)
        my_json = {}
        if type(r) == dict:
            my_json = r
        else:
            my_json = json.loads(r)

        # look for not found error
        #print my_tax_id
        #print my_json
        err = my_json.get("error")
        if err:
            tax_dict['error'] = err
            tax_dict['tax_id'] = 0
            
            if log:
                log.info("- taxonomy error: %s", err)
            else:
                print("- taxonomy error: %s" % err)
            
        else:
            
            for tax_type in tax_dict:
                
                if tax_type in my_json[my_tax_id]:
                    #print "*** %s = %s" % (tax_type, my_json[my_tax_id][tax_type])
                    tax_dict[tax_type] = my_json[my_tax_id][tax_type]['name']
            
            tax_dict['tax_id'] = my_json[my_tax_id].get('tax_id', 0)
            
            if tax_dict['species'] == "-" and tax_dict['genus'] != "-":
                tax_dict['species'] = tax_dict['genus'] + " sp."

        
            

    return tax_dict

'''
Return json of taxonomy
@tax_id = ncbi taxonomy id or ncbi gi
@tax_type = accession, ssu, name, taxid

- look up taxonomy: curl https://taxonomy.jgi.doe.gov/tax/id/9685

'''
def get_taxonomy_service(my_tax_id, tax_type, log=None):

    print_log = False # if log=None, don't print output
    response = None
    err_cnt = 0 # retry up to 6 times
    exit_code = 0
    timeout_sec = 5.0
    retry_sec = 30
    
    #tax_url = "https://taxonomy.jgi-psf.org/"
    tax_url = "https://taxonomy.jgi.doe.gov/"
    #tax_url = "https://taxonomy-dev.jgi-psf.org"
    
    tax_api = "tax/taxid/[tax_id]"
    if tax_type == "gi":
        # RQC-1255 - not supported anymore
        #return {"error" : "GI lookups not supported"}
    
        tax_api = "tax/gi/[tax_id]"
    elif tax_type == "header":
        tax_api = "header/[tax_id]"
        #tax_api = "tax/gi/[tax_id]"
    elif tax_type.startswith("acc"):
        tax_api = "tax/accession/[tax_id]"
        #curl taxonomy.jgi-psf.org/tax/accession/NT_165936.1 -> Aspergillus terreus
    elif tax_type == "ssu":
        #/silvaheader/ will accept a Silva sequence header such as KC415233.1.1497
        tax_api = "tax/silvaheader/[tax_id]"
    elif tax_type == "name":
        tax_api = "tax/name/[tax_id]"
        #http://taxonomy.jgi-psf.org/tax/name/Chitinophaga_sp._str._KP01_AB278570.1

    my_uri = "%s%s" % (tax_url, tax_api)
    my_uri = my_uri.replace("[tax_id]", str(my_tax_id))
    
    if my_tax_id > 0:
        done = False
        
        while not done:
            err = 0
            msg = ""
            try:
                if log:
                    log.info("- tax uri: %s", my_uri)
                else:
                    if print_log:
                        print("- tax uri: %s" % my_uri)
                
                r = requests.get(my_uri, timeout=timeout_sec)
                if r.status_code == 200:
                    done = True
                    response = json.loads(r.text)
                    msg = "- received %s bytes" % len(r.text)
                else:
                    msg = "- status code = %s" % r.status_code
                    
            except requests.exceptions.RequestException as e:
                
                     
                msg = "- GET request failed: %s" % e
                err = 1
    
            except requests.exceptions.Timeout:
                msg = "- GET request timeout out"
                err = 1
            except requests.exceptions.TooManyRedirects:
                msg = "- GET request - too many redirects"
                err = 1
            
            except:
                msg = "- GET request failed for unknown reason"
                # if unknown reason happens, test by commenting this exception out to see the real reason
                err = 1

            if msg:
                if log:
                    log.info(msg)
                else:
                    if print_log:
                        print(msg)

            if err:
                err_cnt += 1
                if err_cnt >= 6:
                    done = True
                else:
                    msg = "- waiting %s seconds to try again, attempt %s" % (retry_sec, err_cnt)
                    if log:
                        log.info(msg)
                    else:
                        if print_log:
                            print(msg)
                    time.sleep(retry_sec)                


    if not response:
        if log:
            log.error("- tax server failure")
        else:
            if print_log:
                print("- tax server failure")
        sys.exit(1)

    return response


'''
Convert dict to an ordered list based on the taxonomy lineage
'''
def format_taxonomy(tax_dict, sep = ";"):

    tax_arr = ['superkingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']
    tax_list = []
    for tax in tax_arr:
        tax_val = "-" # nothing specified
        if tax in tax_dict:
            tax_val = tax_dict[tax]
        else:
            if tax == "superkingdom":
                if 'domain' in tax_dict:
                    tax_val = tax_dict['domain']

        tax_list.append(tax_val)

    #print tax_list

    return sep.join(tax_list)




## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__


if __name__ == "__main__":


    # SSu test
    print("Bacteria_Firmicutes_Clostridia_Clostridiales_Clostridiaceae 1_Clostridium sensu stricto 11_Clostridium acetobutylicum")
    print(format_taxonomy(get_taxonomy("tid|1488|SSU_LZYX01000005.18.1511", "ssu")))
    print()


    print("Rhizobium huautlense OS-49.b AM237359.1 1..1409")
    print(format_taxonomy(get_taxonomy("Rhizobium huautlense OS-49.b AM237359.1 1..1409", "name")))
    print()

    # Felix Catus
    print("Cat: ncbi 9685")
    print(format_taxonomy(get_taxonomy(9685, "ncbi"), "|"))
    print()

    #1001624332 = E.Coli
    print("E.Coli: gi 1001624332")
    print(format_taxonomy(get_taxonomy(1001624332, "gi")))
    print()
    # 0
    print("Nothing: 0")
    print(format_taxonomy(get_taxonomy(0, "ncbi")))
    print()

    # Not sure
    print("? gi: 1104307744")
    print(format_taxonomy(get_taxonomy(1104307744, "gi")))
    print()

    # Shark week (Tiger Shark)
    print("Galeocerdo cuvier")
    print(format_taxonomy(get_taxonomy("Galeocerdo cuvier", "name")))


    sys.exit(0)


