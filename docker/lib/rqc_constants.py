#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Constants that are global for all RQC pipelines.
* don't delete from here, messes up other pipelines
* need to review if these are used at all: 2018-11-15
"""

import os
from common import get_run_path

class RQCConstants:
    UMASK = 0o022 # 755

# Exit codes - don't use this either
class RQCExitCodes:
    # used by run_blastplus_taxserver, run_diamond, readqc, pacbio, dapseq, assemblyqc
    JGI_SUCCESS = 0
    JGI_FAILURE = 1

# The locations of different directories are hard-coded, don-t use this please
# only in assemblyqc
class RQCPaths:
    RQC_SW_BIN_DIR = os.path.join(get_run_path(), "tools")

# General commands - don't use these please
class RQCCommands:

    CAT_CMD = "cat"
    BZCAT_CMD = "bzcat"
    PIGZ_CMD = "pigz" 
    PIGZCAT_CMD = PIGZ_CMD + " -d -c "
    ZCAT_CMD = PIGZCAT_CMD

    RM_CMD = "rm -f" # readqc utils
    SETE_CMD = "set -e" # readqc_utils



# Reference databases
class RQCReferenceDatabases:
    ## NCBI databases
    ## NERSC's DB location: /scratch/blastdb/global/dna/shared/rqc/ref_databases/ncbi/CURRENT"
    rqc_env = os.environ.get("RQC_ENV", "").lower() # set by conda env script rqc27 or rqc36 (RQC-1314)
    
    
    # 2019-09-11 - everything with a os.path.join is copied to qc aws instance (except ncbi/CURRENT)
    
    ref_db_path = "/global/dna/shared/rqc/ref_databases"
    ncbi_path = "/global/dna/shared/rqc/ref_databases/ncbi/CURRENT"
    #ncbi_path = "/global/dna/shared/rqc/ref_databases/ncbi/20190408"

    if rqc_env == "aws":
        ref_db_path = "/home/ec2-user/db/ref_databases"
        ncbi_path = "/home/ec2-user/db/ncbi"
   
    NR                   = os.path.join(ncbi_path, "nr/nr")
    NR_LAST = os.path.join(ncbi_path, "nr/LAST/nr")
    NR_DIAMOND = os.path.join(ncbi_path, "nr/diamond/nr")
    NR_DIAMOND_CSCRATCH = "/global/cscratch1/sd/qc_user/dna/shared/rqc/ref_databases/ncbi/CURRENT/nr/diamond/nr" # NERSC only
    REFSEQ_ARCHAEA       = os.path.join(ncbi_path, "refseq.archaea/refseq.archaea")
    REFSEQ_BACTERIA      = os.path.join(ncbi_path, "refseq.bacteria/refseq.bacteria")
    REFSEQ_FUNGI         = os.path.join(ncbi_path, "refseq.fungi/refseq.fungi")
    REFSEQ_MITOCHONDRION = os.path.join(ncbi_path, "refseq.mitochondrion/refseq.mitochondrion")
    REFSEQ_PLANT         = os.path.join(ncbi_path, "refseq.plant/refseq.plant")
    REFSEQ_PLASMID       = os.path.join(ncbi_path, "refseq.plasmid/refseq.plasmid")
    REFSEQ_PLASTID       = os.path.join(ncbi_path, "refseq.plastid/refseq.plastid")
    REFSEQ_VIRAL         = os.path.join(ncbi_path, "refseq.viral/refseq.viral")
    NT                   = os.path.join(ncbi_path, "nt/nt")

    # should use this by default for NT
    NT_maskedYindexedN_BB = os.path.join(ncbi_path, "nt/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted")

    ## Non NCBI databases
    
    MICROBES = os.path.join(ref_db_path, "fusedERPBBmasked.fa.gz") # BB common microbial contamination @ JGI
    COLLAB_16S = os.path.join(ref_db_path, "collab16s.fa")
    SCICLONE_RNA = os.path.join(ref_db_path, "sciclone_rna.fa")
    SCICLONE_DNA = os.path.join(ref_db_path, "sciclone_dna.fa")
    KAPA = os.path.join(ref_db_path, "kapatags.L40.fa")
    ANTIFAM_HMM = os.path.join(ref_db_path, "antifam/AntiFam.hmm")
    
    # To do: check for existence of dbs in this file - test condition
    # 2 different path setups: !=nersc is how things should be setup
    
    if rqc_env != "nersc":
        # sagtaminants = ribo-removed list of contam for microbial pipelines
        SAGTAMINANTS = os.path.join(ref_db_path, "sagtaminants.ribomask.fa")
        MICROBIAL_WATCHLIST = os.path.join(ref_db_path, "microbial_watchlist.201803.fa")
        
        #GREEN_GENES = "/global/projectb/sandbox/rqc/blastdb/green_genes/2011.04.green_genes16s/green_genes16s.insa_gg16S.fasta" # aug 2013
        GREEN_GENES  = os.path.join(ref_db_path, "green_genes16s.insa_gg16S.fasta") # same file as above but has db index

        NCBI_GCONTAM = os.path.join(ref_db_path, "ncbi_prescreen_gcontam.fa") # NCBI contaminations screen, from Sept 2013
        CONTAMINANTS = os.path.join(ref_db_path, "JGIContaminants.fa") # april 2015

        ARTIFACT_NO_SPIKEIN  = os.path.join(ref_db_path, "Illumina.artifacts.2013.12.no_DNA_RNA_spikeins.fa")
        ARTIFACT_DNA_SPIKEIN = os.path.join(ref_db_path, "DNA_spikeins.artifacts.2012.10.1.fa") #this db has just DNA spike-in sequences
        ARTIFACT_RNA_SPIKEIN = os.path.join(ref_db_path, "RNA_spikeins.artifacts.2012.10.NoPolyA.fa") #this db has just RNA spike-in sequences
        ARTIFACT_PRIMER_ONLY = os.path.join(ref_db_path, "Artifacts.adapters_primers_only.fa")
        
        FOSMID_VECTOR = os.path.join(ref_db_path, "pCC1Fos.ref.fa")
        RRNA = os.path.join(ref_db_path, "rRNA.fa") ## including Chlamy general RRNA file


        # Silva
        LSU_REF = os.path.join(ref_db_path, "silva/CURRENT/LSURef_tax_silva")
        SSU_REF = os.path.join(ref_db_path, "silva/CURRENT/SSURef_tax_silva")
        LSSU_REF = os.path.join(ref_db_path, "silva/CURRENT/LSSURef_tax_silva")
        SILVA = os.path.join(ref_db_path, "silva_132_deduped_sorted.fa.gz")

        ## Alignment
        ARTIFACT_REF = os.path.join(ref_db_path, "Artifacts.adapters_primers_only.fa")

        ## PHiX Pipeline
        PHIX_REF = os.path.join(ref_db_path, "phix174_ill.ref.fa")
        PHIX_CIRCLE_REF = os.path.join(ref_db_path, "phix174.ilmn_ref_concat.fa")
        FRAG_ADAPTERS = os.path.join(ref_db_path, "adapters.fa")
    
        # Pacbio 2kb control
        CONTROL_2KB = os.path.join(ref_db_path, "2kb_control.fasta")
        CONTROL_4KB = os.path.join(ref_db_path, "4kb_Control.fasta")
        GCONTAM_2KB = os.path.join(ref_db_path, "gcontam+2kb_control")
    
        KRAKEN_NT = "" #"/global/projectb/sandbox/rqc/prod/kraken/CURRENT-nt/"
    
        # Fungal
        UNITE = "" #"/global/dna/shared/databases/qaqc_db/unite/UNITE"
    
        # Cloning Vectors?
        UNIVEC = os.path.join(ref_db_path, "ncbi/CURRENT/UniVec/UniVec")
    
    else:
        
        # sagtaminants = ribo-removed list of contam for microbial pipelines
        #SAGTAMINANTS = "/global/dna/shared/rqc/ref_databases/ncbi/sag/sagtaminants.fa"
        SAGTAMINANTS = os.path.join(ref_db_path, "ncbi/sag/sagtaminants.ribomask.fa")
        # microbial_watchlist - replaced sagtaminants
        MICROBIAL_WATCHLIST = os.path.join(ref_db_path, "ncbi/sag/microbial_watchlist.201803.fa")
 
        #GREEN_GENES = "/global/projectb/sandbox/rqc/blastdb/green_genes/2011.04.green_genes16s/green_genes16s.insa_gg16S.fasta" # aug 2013
        GREEN_GENES  = os.path.join(ref_db_path, "misc/CURRENT/green_genes16s.insa_gg16S.fasta") # same file as above but has db index

        #/global/dna/shared/rqc/ref_databases/misc/CURRENT/JGIContaminants.fa
        CONTAMINANTS = os.path.join(ref_db_path, "misc/CURRENT/JGIContaminants.fa") # april 2015
        #/global/projectb/sandbox/rqc/qcdb/JGIContaminants/JGIContaminants.fa = aug 2013 - no

        # NCBI contamination screen
        NCBI_GCONTAM = os.path.join(ref_db_path, "qaqc/databases/ncbi_prescreen_gcontam.fa")
        #COLLAB16S = "/global/projectb/sandbox/rqc/qcdb/collab16s/collab16s.fa" # recent. Maintained by Kecia, created symlink in ref_db_path
        
        ARTIFACT_NO_SPIKEIN  = os.path.join(ref_db_path, "qaqc/databases/illumina.artifacts/Illumina.artifacts.2013.12.no_DNA_RNA_spikeins.fa") #aka synthetic
        ARTIFACT_DNA_SPIKEIN = os.path.join(ref_db_path, "qaqc/databases/illumina.artifacts/DNA_spikeins.artifacts.2012.10.1.fa") #this db has just DNA spike-in sequences
        ARTIFACT_RNA_SPIKEIN = os.path.join(ref_db_path, "qaqc/databases/illumina.artifacts/RNA_spikeins.artifacts.2012.10.NoPolyA.fa") #this db has just RNA spike-in sequences
        ARTIFACT_PRIMER_ONLY = os.path.join(ref_db_path, "qaqc/databases/Artifacts.adapters_primers_only.fa")



        FOSMID_VECTOR = os.path.join(ref_db_path, "qaqc/databases/pCC1Fos.ref.fa")
        #PHIX = os.path.join(ref_db_path, "qaqc/databases/phix174_ill.ref.fa")
        RRNA = os.path.join(ref_db_path, "qaqc/databases/rRNA.fa") ## including Chlamy



        # Silva
        LSU_REF = os.path.join(ref_db_path, "silva/CURRENT/LSURef_tax_silva")
        SSU_REF = os.path.join(ref_db_path, "silva/CURRENT/SSURef_tax_silva")
        LSSU_REF = os.path.join(ref_db_path, "silva/CURRENT/LSSURef_tax_silva")
        SILVA = os.path.join(ref_db_path, "qaqc/databases/silva_132_deduped_sorted.fa.gz")
        
        ## Alignment
        ARTIFACT_REF = os.path.join(ref_db_path, "qaqc/databases/Artifacts.adapters_primers_only.fa")

        ## PHiX Pipeline
        PHIX_REF = os.path.join(ref_db_path, "qaqc/databases/phix174_ill.ref.fa")
        PHIX_CIRCLE_REF = os.path.join(ref_db_path, "qaqc/databases/phix174.ilmn_ref_concat.fa")
        FRAG_ADAPTERS = os.path.join(ref_db_path, "qaqc/databases/adapters.fa")
    
        # Pacbio 2kb control
        CONTROL_2KB = os.path.join(ref_db_path, "qaqc/databases/2kb_control.fasta")
        CONTROL_4KB = os.path.join(ref_db_path, "qaqc/databases/4kb_Control.fasta")
        GCONTAM_2KB = os.path.join(ref_db_path, "ncbi/gcontam+2kb_control/gcontam+2kb_control")
    
        KRAKEN_NT = "/global/projectb/sandbox/rqc/prod/kraken/CURRENT-nt/"
    
        # Fungal
        UNITE = "/global/dna/shared/databases/qaqc_db/unite/UNITE"
    
        # Cloning Vectors?
        UNIVEC = os.path.join(ref_db_path, "ncbi/CURRENT/UniVec/UniVec")
    
    
## EOF
